import UIKit
import app

class ViewController: UIViewController, GameView {

    @IBOutlet private weak var label: UILabel!
    @IBOutlet private weak var buttons: UIStackView!
    @IBOutlet private weak var replayButton: UIButton!
    
    private var currentPlayer = 1
    
    private lazy var gamePresenter = GamePresenter(gameView: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        gamePresenter.startGame()
    }

    func showTie() {
        label.text = NSLocalizedString("tie", comment: "tie")
        showMoveButtons(visible: false)

    }
    
    func showViewForPlayerOne() {
        currentPlayer = 1
        label.text = NSLocalizedString("player1turn", comment: "player 2")
        showMoveButtons(visible: true)
    }
    
    func showViewForPlayerTwo() {
        currentPlayer = 2
        label.text = NSLocalizedString("player2turn", comment: "player 1")
        showMoveButtons(visible: true)
    }
    
    func showWinner(winner: Int32) {
        label.text =  String.localizedStringWithFormat( NSLocalizedString("%d winner", comment: "winner"), winner)
        showMoveButtons(visible: false)
    }
    
    private func showMoveButtons(visible: Bool) {
        buttons.isHidden = !visible
        replayButton.isHidden = visible
    }
    
    private func registerMove(move: Move) {
        if (currentPlayer == 1) {
            gamePresenter.setMoveForPlayerOne(move: move)
        } else if (currentPlayer == 2) {
            gamePresenter.setMoveForPlayerTwo(move: move)
        }
    }
    
    @IBAction
    func rockClicked() {
        registerMove(move: Move.rock)
    }
 
    @IBAction
    func scissorClicked() {
        registerMove(move: Move.scissors)
    }
    
    @IBAction
    func paperClicked() {
        registerMove(move: Move.paper)
    }
    
    @IBAction
    func restartGame() {
        gamePresenter.startGame()
    }
}
