package be.matthiasdruwe.rockpaperscissors

import kotlin.test.BeforeTest
import kotlin.test.Test
import kotlin.test.assertEquals

class GamePresenterTest {

    private lateinit var gameView: GameViewStub
    private lateinit var gamePresenter: GamePresenter

    @BeforeTest
    fun setup() {
        gameView = GameViewStub()
        gamePresenter = GamePresenter(gameView)
    }

    @Test
    fun `load game when screen called`() {
        gamePresenter.startGame()
        assertEquals(1, gameView.showViewForPlayerOneCalledCount)
    }

    @Test
    fun `when only set movePlayer1 show view for player 2`() {
        gamePresenter.setMoveForPlayerOne(MoveMock.Wins)
        assertEquals(1, gameView.showViewForPlayerTwoCalledCount)
        assertEquals(0, gameView.showWinnerIsCalledCount)
        assertEquals(0, gameView.showTieCalledCount)
    }

    @Test
    fun `when only set movePlayer2 show view for player 1`() {
        gamePresenter.setMoveForPlayerTwo(MoveMock.Wins)
        assertEquals(1, gameView.showViewForPlayerOneCalledCount)
        assertEquals(0, gameView.showWinnerIsCalledCount)
        assertEquals(0, gameView.showTieCalledCount)
    }

    @Test
    fun `show winner 1 if player 1 wins`() {
        gamePresenter.setMoveForPlayerOne(MoveMock.Wins)
        gamePresenter.setMoveForPlayerTwo(MoveMock.Loses)

        assertEquals(1, gameView.lastWinner)
    }

    @Test
    fun `show winner 2 if player 2 wins`() {
        gamePresenter.setMoveForPlayerOne(MoveMock.Loses)
        gamePresenter.setMoveForPlayerTwo(MoveMock.Wins)

        assertEquals(2, gameView.lastWinner)
    }

    @Test
    fun `don't call view for player 2 if player 2 already have an item clicked`() {
        gamePresenter.setMoveForPlayerTwo(MoveMock.Wins)
        gamePresenter.setMoveForPlayerOne(MoveMock.Wins)

        assertEquals(1, gameView.showWinnerIsCalledCount)
        assertEquals(0, gameView.showViewForPlayerTwoCalledCount)
    }

    @Test
    fun `don't call view for player 1 if player 1 already have an item clicked`() {
        gamePresenter.setMoveForPlayerOne(MoveMock.Wins)
        gamePresenter.setMoveForPlayerTwo(MoveMock.Wins)

        assertEquals(1, gameView.showWinnerIsCalledCount)
        assertEquals(0, gameView.showViewForPlayerOneCalledCount)
    }

    @Test
    fun `start next round when start is clicked`() {
        gamePresenter.startGame()
        gamePresenter.setMoveForPlayerOne(MoveMock.Wins)
        gamePresenter.setMoveForPlayerTwo(MoveMock.Wins)
        gamePresenter.startGame()
        gamePresenter.setMoveForPlayerOne(MoveMock.Wins)
        gamePresenter.setMoveForPlayerTwo(MoveMock.Wins)

        assertEquals(2, gameView.showViewForPlayerOneCalledCount)
        assertEquals(2, gameView.showViewForPlayerTwoCalledCount)
        assertEquals(2, gameView.showWinnerIsCalledCount)
    }


    class GameViewStub : GameView {
        var showViewForPlayerOneCalledCount = 0
        var showViewForPlayerTwoCalledCount = 0
        var showWinnerIsCalledCount = 0
        var lastWinner = 0
        var showTieCalledCount = 0

        override fun showViewForPlayerOne() {
            showViewForPlayerOneCalledCount++
        }

        override fun showViewForPlayerTwo() {
            showViewForPlayerTwoCalledCount++
        }

        override fun showWinner(winner: Int) {
            showWinnerIsCalledCount++
            lastWinner = winner
        }

        override fun showTie() {
            showTieCalledCount++
        }
    }

    enum class MoveMock: Action {
        Wins,
        Loses,
        Tie;

        override fun winsFrom(move: Action): Boolean? {
            return when (this) {
                Wins -> true
                Loses -> false
                Tie -> null
            }
        }
    }
}

