package be.matthiasdruwe.rockpaperscissors

import kotlin.test.*


class MoveTest {

    @Test
    fun `paper beats rock`() {
        checkWinner(Move.Paper, Move.Rock, Move.Paper)
    }

    @Test
    fun `rock beats scissors`() {
        checkWinner(Move.Rock, Move.Scissors, Move.Rock)
    }

    @Test
    fun `scissors beats paper`() {
        checkWinner(Move.Scissors, Move.Paper, Move.Scissors)
    }

    @Test
    fun `tie when both are scissors` () {
        checkTie(Move.Scissors)
    }

    @Test
    fun `tie when both are rock` () {
        checkTie(Move.Rock)
    }

    @Test
    fun `tie when both are paper` () {
        checkTie(Move.Paper)
    }

    private fun checkWinner(move1: Move, move2: Move, winner: Move) {
        val result = move1.winsFrom(move2)
        val invertedResult = move2.winsFrom(move1)

        assertNotNull(result)
        assertNotNull(invertedResult)

        if(move1 == winner) {
            assertTrue(result)
            assertFalse(invertedResult)
        } else {
            assertFalse(result)
            assertTrue(invertedResult)
        }
    }

    private fun checkTie(move: Move) {
        val tie = move.winsFrom(move)

        assertNull(tie)
    }
}
