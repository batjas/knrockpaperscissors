package be.matthiasdruwe.rockpaperscissors

interface Action {
    fun winsFrom(move: Action): Boolean?
}

enum class Move : Action {
    Rock,
    Paper,
    Scissors;

    override fun winsFrom(move: Action): Boolean? {
        if (this == move) {
            return null
        }

        return when(this) {
            Rock -> move == Scissors
            Scissors -> move == Paper
            Paper -> move == Rock
        }
    }
}
