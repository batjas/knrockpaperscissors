package be.matthiasdruwe.rockpaperscissors

interface GameView {
    fun showViewForPlayerOne()
    fun showViewForPlayerTwo()
    fun showWinner(winner: Int)
    fun showTie()
}

class GamePresenter(private val gameView: GameView) {
    private var movePlayer1: Action? = null
    private var movePlayer2: Action? = null

    fun startGame() {
        movePlayer1 = null
        movePlayer2 = null
        gameView.showViewForPlayerOne()
    }

    fun setMoveForPlayerOne(move: Action) {
        movePlayer1 = move

        if(movePlayer2 == null) {
            gameView.showViewForPlayerTwo()
        } else {
            calculateWinner()
        }
    }

    fun setMoveForPlayerTwo(move: Action) {
        movePlayer2 = move

        if (movePlayer1 == null) {
            gameView.showViewForPlayerOne()
        } else {
            calculateWinner()
        }
    }

    private fun calculateWinner() {
        val movePlayer1 = movePlayer1
        val movePlayer2 = movePlayer2

        if (movePlayer1 != null && movePlayer2 != null) {
            val result = movePlayer1.winsFrom(movePlayer2)

            if (result != null) {
                if (result) {
                    gameView.showWinner(1)
                } else {
                    gameView.showWinner(2)
                }
            } else {
                gameView.showTie()
            }
        }
    }

}
