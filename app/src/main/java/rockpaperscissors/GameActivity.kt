package be.matthiasdruwe.rockpaperscissors

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.game_activity.*
import rockpaperscissors.R


class GameActivity : AppCompatActivity(), GameView {

    private val gamePresenter = GamePresenter(this)
    private var currentPlayer = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_activity)
        setupListeners()
        gamePresenter.startGame()
    }

    private fun setupListeners() {
        buttonRock.setOnClickListener { registerMove(Move.Rock) }
        buttonPaper.setOnClickListener { registerMove(Move.Paper) }
        buttonScissors.setOnClickListener { registerMove(Move.Scissors) }
        buttonNewGame.setOnClickListener { gamePresenter.startGame() }
    }

    private fun registerMove(move: Move) {
        if (currentPlayer == 1) {
            gamePresenter.setMoveForPlayerOne(move)
        } else if (currentPlayer == 2) {
            gamePresenter.setMoveForPlayerTwo(move)
        }
    }

    override fun showViewForPlayerOne() {
        currentPlayer = 1
        textViewPlayer.setText(R.string.player1turn)
        showMoveButtons(true)
        showRestartGameButton(false)
    }

    override fun showViewForPlayerTwo() {
        currentPlayer = 2
        textViewPlayer.setText(R.string.player2turn)
        showMoveButtons(true)
        showRestartGameButton(false)

    }

    override fun showWinner(winner: Int) {
        textViewPlayer.text = resources.getString(R.string.winner, winner)
        showMoveButtons(false)
        showRestartGameButton(true)
    }

    override fun showTie() {
        textViewPlayer.setText(R.string.tie)
        showMoveButtons(false)
        showRestartGameButton(true)
    }

    private fun showMoveButtons(visible: Boolean) {
        buttonRock.isVisible = visible
        buttonPaper.isVisible = visible
        buttonScissors.isVisible = visible
    }

    private fun showRestartGameButton(visible: Boolean) {
        buttonNewGame.isVisible = visible
    }
}
